--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `gram_id` varchar(32) NOT NULL,
  `created_time` int(10) NOT NULL,
  `location` varchar(255) NOT NULL,
  `standard_url` varchar(255) NOT NULL,
  `hires_video_url` varchar(255) NOT NULL,
  `user_profile_picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`gram_id`),
  KEY `created_time` (`created_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE `votes` (
  `gram_id` varchar(32) NOT NULL,
  `votes` smallint(3) NOT NULL DEFAULT '0',
  KEY `gram_id` (`gram_id`),
  KEY `votes` (`votes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
